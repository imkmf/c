#include <stdio.h>
#include <string.h>

int main(void) {
  char s[] = "My test string";
  int length = strlen(s);
  char reversed[length];

  int i;

  for (i = 0; i < length; ++i) {
    reversed[length - i] = s[i];
  }

  printf("%s", reversed);

  return 0;
}
