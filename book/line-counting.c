#include <stdio.h>

/* count lines in input */

/* int main(void) */
/* { */
/*   int c, nl; */

/*   nl = 0; */
/*   while ((c = getchar()) != EOF) */
/*     if (c == '\n') */
/*       ++nl; */
/*   printf("%d\n", nl); */
/* } */

/* Exercise 1.8 */

int main(void)
{
  int c, blank, tab, nl;

  blank = 0;
  tab = 0;
  nl = 0;
  while ((c = getchar()) != EOF) {
    if (c == ' ')
      ++blank;
    if (c == '\t')
      ++tab;
    if (c == '\n')
      ++nl;
  }
  printf("newlines: %d\n", nl);
  printf("tabs: %d\n", tab);
  printf("blanks: %d\n", blank);
}
