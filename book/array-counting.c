#include <stdio.h>

/* count digits, white space, others */
/* int main(void) */
/* { */
/*   int c, i, nwhite, nother; */
/*   int ndigit[10]; */

/*   nwhite = nother = 0; */
/*   for (i = 0; i < 10; ++1) */
/*     ndigit[i] = 0; */

/*   while ((c = getchar()) != EOF) */
/*     /\* Check that the character set value of c */
/*        is between 0 and 9 (a digit): */

/*        '0' - 0 */
/*        '1' - 1 */
/*        ... */
/*        '9' - 9 */
/*     *\/ */

/*     if (c >= '0' && c <= '9') */
/*       ++ndigit[c-'0']; */
/*     else if (c == ' ' || c == '\n' || c == '\t') */
/*       ++white; */
/*     else */
/*       ++nother; */

/*   printf("digits ="); */
/*   for (i = 0; i < 10; ++i) */
/*     printf(" %d", ndigit[i]); */
/*   printf(", white space = %d, other = %d\n", */
/*          nwhite, nother); */
/* } */

/* Exercise 1-13 */
/* int main(void) */
/* { */
/*   int c, i, j, nother; */
/*   int ndigit[10]; */

/*   nother = 0; */
/*   for (i = 0; i < 10; ++i) */
/*     ndigit[i] = 0; */

/*   while ((c = getchar()) != EOF) */
/*     if (c == ' ' || c == '\n' || c == '\t') { */
/*       ++ndigit[nother]; */
/*       nother = 0; */
/*     } else { */
/*       ++nother; */
/*     } */

/*   for (i = 0; i < 10; ++i) { */
/*     printf("%d|", i); */
/*     for (j = 0; j < ndigit[i]; ++j) { */
/*       printf("="); */
/*     } */
/*     printf("\n"); */
/*   } */
/* } */

/* Exercise 1-14 */
/* h/t http://stackoverflow.com/q/1616706 */

#include <ctype.h>

#define LIMIT 'z'-' '

int main(void)
{
  int c, i, j, nother;
  int nchars[LIMIT] = {0};

  while ((c = getchar()) != EOF) {
    if (c != ' ' || c != '\n' || c != '\t') {
      /*
        Get the letter within the character range
        as defined by LIMIT
      */
      ++nchars[c - ' '];
    }
  }

  for (i = 1; i < LIMIT; ++i) {
    printf("%i|", i);
    for (j = 0; j < nchars[i]; ++j) {
      printf("=");
    }

    printf("\n");
  }
}
