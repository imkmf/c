#include <stdio.h>
#define MAXLINE 1000

int kf_getline(char line[], int max);
int strindex(char source[], char searchfor[]);

char pattern[] = "ould";

int main(void) {
  char line[MAXLINE];
  int found = 0;

  // There is a line
  while (kf_getline(line, MAXLINE) > 0) {
    // It has "pattern" in it
    int index = strindex(line, pattern);
    if (index >= 0) {
      // Print it
      printf("%s", line);
      // Found in another line
      found++;
    }

    return found;
  }

  return 0;
}

int kf_getline(char string[], int max) {
  int gotchar, sindex;
  sindex = 0;
  while (--max > 0 && (gotchar = getchar()) != EOF && gotchar != '\n') {
    string[sindex++] = gotchar;
  }
  if (gotchar == '\n') {
    string[sindex++] = gotchar;
  }
  string[sindex] = '\0';
  return sindex;
}

int strindex(char string[], char tofind[]) {
  int sindex, position, tindex;
  for (sindex = 0; string[sindex] != '\0'; sindex++) {
    for (position = sindex, tindex = 0; tofind[tindex] != '\0' && string[position] == tofind[tindex]; position++, tindex++)
      ;
    if (tindex > 0 && tofind[tindex] == '\0') {
      return sindex;
    }
  }

  return -1;
}
