#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXWORD 100

#define SIZE 50
#define BUFSIZE 100

char buf[BUFSIZE];
int bufp = 0;

int getch(void) /* get a (possibly pushed-back) character */
{
  return (bufp > 0) ? buf[--bufp] : getchar();
}

void ungetch(int c) /* push character back on input */
{
  if (bufp >= BUFSIZE) {
    printf("ungetch: too many characters\n");
  } else {
    buf[bufp++] = c;
  }
}

/* getword:  get next word or character from input */
int getword(char *word, int lim) {
  int c, getch(void);
  void ungetch(int);
  char *w = word;

  while (isspace(c = getch()))
    ;

  if (c != EOF) {
    *w++ = c;
  }

  if (!isalpha(c)) {
    *w = '\0';
    return c;
  }

  for ( ; --lim > 0; w++) {
    if (!isalnum(*w = getch())) {
      ungetch(*w);
      break;
    }
  }

  *w = '\0';
  return word[0];
}

struct tnode {         /* the tree node */
  char *word;            /* points to the text */
  int count;             /* number of occurrences */
  struct tnode *left;    /* left child */
  struct tnode *right;   /* right child */
};

struct tnode *talloc(void) {
  return (struct tnode *) malloc(sizeof(struct tnode));
}

char *kf_strdup(char *s) {  /* make a dup of s */
  char *p;

  p = (char *) malloc(strlen(s) + 1);  /* +1 for '\0' */
  if (p != NULL) {
    strcpy(p, s);
  }

  return p;
}

struct tnode *addtree(struct tnode *p, char *w) {
  int cond;

  if (p == NULL) {        /* A new word has arrived */
    p = talloc();         /* Make a new node */
    p->word = kf_strdup(w);
    p->count = 1;
    p->left = p->right = NULL;
  } else if ((cond = strcmp(w, p->word)) == 0) {
    p->count++;           /* repeated word */
  } else if (cond < 0) {  /* less than into left subtree */
    p->left = addtree(p->left, w);
  } else {                /* greater than into right subtree */
    p->right = addtree(p->right, w);
  }
  return p;
}

void treeprint(struct tnode *p) {
  if (p != NULL) {
    treeprint(p->left);
    printf("%4d %s\n", p->count, p->word);
    treeprint(p->right);
  }
}

int main(void) {
  struct tnode *root;
  char word[MAXWORD];

  root = NULL;
  while (getword(word, MAXWORD) != EOF) {
    if (isalpha(word[0])) {
      root = addtree(root, word);
    }
  }

  treeprint(root);
  return 0;
}
