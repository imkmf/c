#include <stdio.h>
#define MAXLINE 1000

int main(void) {
  char string[MAXLINE];
  int c;
  int toAssign;
  int i = 0;
  while ((c = getchar()) != EOF && c != '\n') {
    /* Do an extra stop and replace the tab value
       so we get two spaces in the string */
    if (c == '\t') {
      string[i] = ' ';
      ++i;
      c = ' ';
    }

    string[i] = c;
    ++i;
  }

  printf("%s", string);
}
