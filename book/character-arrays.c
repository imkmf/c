#include <stdio.h>
#include <string.h>
#define MAXLINE 1000

int kf_getline(char s[], int lim);
void copy(char to[], char from[]);

int main(void) {
  int len;                /* Current line length */
  int max;                /* Maximum length seen so far */
  char line[MAXLINE];     /* Current input line */
  char longest[MAXLINE];  /* Longest line saved here */

  max = 0;
  /* proxy gathering input to kf_getline */
  while ((len = kf_getline(line, MAXLINE)) > 0) {
    /* this is larger than we've seen before */
    if (len > max) {
      /* this is the new largest */
      max = len;
      copy(longest, line);
    }

    /* Exercise 1-17 */
    if (len > 80) {
      printf("Long line! %s\n", line);
    }
  }

  if (max > 0) {  /* there was a line */
    /* printf("longest is %s", longest); */

    /* Exercise 1-18 */
    printf("Length: %i\nLine: %s\n", max, longest);
  }

  return 0;
}

/* getline: read a line into s, return length */

/*
   Construct an array and push elements onto it,
   up to size lim.

   This function mutates s[] to be used in the copy function later
*/
int kf_getline(char s[], int lim) {
  int c, i;

  for (i=0; i < lim-1 && (c=getchar())!=EOF && c!='\n'; ++i) {
    /* This isn't a "end" character (new-line or EOF),
       add it to the array */
    s[i] = c;
  }
  if (c == '\n') {
    /* this is an end character,
       add it to the array and push length manually */
    s[i] = c;
    ++i;
  }
  /* terminate array */
  s[i] = '\0';
  /* return length */
  return i;
}

/* copy: copy 'from' into 'to'; assume to is big enough */
void copy(char to[], char from[]) {
  int i;

  i = 0;
  /* Walk through the arrays,
     assigning to[i] to the value of from[i].
     If the terminator value appears, stop. */
  while ((to[i] = from[i]) != '\0')
    ++i;
}
